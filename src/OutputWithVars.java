/*
   Name: Paul Cleworth
   Date: 8/29/2018
*/

import java.util.Scanner;

public class OutputWithVars {
    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);
        int userNum;
        int userNum2;
        int userNum3;

        System.out.println("Enter integer:");
        userNum = scnr.nextInt();

        System.out.println("You entered: " + userNum);
        System.out.println(userNum + " squared is " + userNum * userNum);
        System.out.println("And " + userNum + " cubed is " + userNum * userNum * userNum + "!!");

        System.out.println("Enter another integer:");
        userNum2 = scnr.nextInt();
        userNum3 = userNum + userNum2;

        System.out.println(userNum + " + " + userNum2 + " is " + userNum3);
        System.out.println(userNum + " * " + userNum2 + " is " + userNum * userNum2);

    }
}
