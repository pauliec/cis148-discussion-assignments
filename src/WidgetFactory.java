/*
    Name: Tyler Mallett
    Date: 10-17-2018
    This application will demonstrate the creation of a collection of objects.
    There will be input to create individual widget objects that contain a name field and a price.

    Team Member with Branch: Paul Cleworth, 10/29/2018

 */

import java.util.Scanner;

public class WidgetFactory {

    public static void main(String[] args) {
        /*
            - Setup a scanner object to read user input
            - Use a loop to
                    - prompt the user to input a widget name and price
                    - store values in a new widget
                    - add the widget to a widget array
                    - update the widget count
            - You can only create up to the maximum of widgets allowed
            - Display the collection of widgets and the average price
         */

        Scanner input = new Scanner(System.in);
        String name = "";
        double price = 0;
        Widget newWidget = new Widget();
        Widget widgets[] = new Widget[Widget.getMaxWidgets()];
        double totalPrice = 0;
        int i = 0;

        // While loop to ensure number of widgets does not exceed MAX_WIDGETS
        while (Widget.getcount() < Widget.getMaxWidgets()) {

            // Get widget name
            System.out.println("Enter a widget name: ");
            name = input.nextLine();

            // Get widget price
            System.out.println("Enter the widget price: ");
            price = input.nextDouble();

            // Reset buffer to accept a string
            input.nextLine();

            // Create new widget
            newWidget = new Widget(name, price);

            // Store new widget in the array
            widgets[Widget.getcount()] = newWidget;
            Widget.updateCount();

        }

        // For loop to print out widget names
        for (i = 0; i < Widget.getMaxWidgets(); i++){
            System.out.println("Widget " + (i + 1) + ": " + widgets[i].getName());
        }

        // For loop to find total price for all widgets
        for (i = 0; i < Widget.getMaxWidgets(); i++){
            totalPrice += widgets[i].getPrice();
        }

        // Calculate and print the  average widget price
        System.out.printf("Average widget price: $%.2f\n", (totalPrice / Widget.getMaxWidgets()));


    }
}